When the 3rd MOOC in the Android App Development Specialization is
complete this folder will contain a material design-based
implementation of an ImageDownloader app using Android Studio.  Until
then, you can check out the Eclipse-based version that's available
[here](https://github.com/douglascraigschmidt/POSA-14/tree/master/ex/DownloadApplication).
